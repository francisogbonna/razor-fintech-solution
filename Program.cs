﻿using System;
using System.Reflection;
using RazorFintech.Model;
using System.Threading.Tasks;

namespace RazorFintech
{
    class Program
    {
        static async Task Main(string[] args)
        {
            await  Services.RunAsync();
        }
    }
}
