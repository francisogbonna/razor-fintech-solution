﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RazorFintech.Model
{
    public class UserModelDB
    {
        public async Task<IEnumerable<UserModel>> GetUserDetailsAsync()
        {
            var Customers = new List<UserModel>()
            {
                new UserModel
                {
                    FullName="Pope Francis",AccountType="Current Account",
                    AccountBalance=4_000_000,Email="pope@gmail.com",Address="Genesystech hub"
                },
                new UserModel
                {
                FullName="Friendly Francis",AccountType="Fixed Deposit Account",
                AccountBalance=14_000_000,Email="frank@gmail.com",Address="Bezao Hub"
                },
                new UserModel
                {
                FullName="Terrible Tochukwu",AccountType="Savings Account",
                AccountBalance=4_000,Email="terror@gmail.com",Address="Obiagu"
                },
            };
            await Task.Delay(3000);
            return Customers;
        }
    }
}
