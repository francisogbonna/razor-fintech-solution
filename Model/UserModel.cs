﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RazorFintech.Model
{
    public class UserModel
    {
        public string FullName { get; set; }
        public string AccountType { get; set; }
        public decimal AccountBalance { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        
    }
}
