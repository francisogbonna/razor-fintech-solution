﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace RazorFintech.Model
{
    public class AccountService
    {
        AccountModelDB accountDB = new AccountModelDB();
        UserModelDB userDB = new UserModelDB();

        public async Task<string> AuthenticationAsync(string username)
        {
            var details = await userDB.GetUserDetailsAsync();

            var user =  from customer in details where customer.FullName.Equals(username)select customer;
            StringBuilder stringBulder = new StringBuilder();
            foreach (var item in user)
            {
                stringBulder.Append($"Account Type: {item.AccountType} \nAccount Balance: {item.AccountBalance} \nEmail: {item.Email} \nAddress: {item.Address}");
            }
            
            return stringBulder.ToString();
        }
        
        public async Task<string> LoginAsync(string name, string password)
        {
            var usersList = await accountDB.GetAccountDetailsAsync();
            foreach (var item in usersList)
            {
                if(name.ToLower() ==item.fullName.ToLower() && password == item.Password)
                {
                    return $"{item.fullName} {item.AccountNo}";
                }
                else
                {
                    return "User not found!";
                }
            }
            return "Error: Network failed.";
        }
    }
}