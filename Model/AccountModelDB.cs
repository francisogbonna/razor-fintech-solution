﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RazorFintech.Model
{
    public class AccountModelDB
    {
        public async  Task<IEnumerable<AccountModel>> GetAccountDetailsAsync()
        {
            var accounts = new List<AccountModel>()
            {    
                new AccountModel{AccountNo="223456789",fullName="Pope Francis",Password="pope"},
                new AccountModel{AccountNo="223456789",fullName="Friendly Francis",Password="12345"},
                new AccountModel{AccountNo="223456789",fullName="Terrible Tochukwu",Password="terror"},
            };
            await Task.Delay(3000);
            return accounts;
        }  
    }
}
