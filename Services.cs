﻿using System;
using System.Collections.Generic;
using System.Text;
using RazorFintech.Model;
using System.Threading.Tasks;

namespace RazorFintech
{
    public class Services
    {
        public static async Task RunAsync()
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("\tWelcome to Razor Fintech Solutions Ltd.");
            Console.ResetColor();
            Console.WriteLine("Enter details to login and view profile.");
            Console.WriteLine("Enter Full Name:");
            string userName = Console.ReadLine();
            Console.WriteLine("Enter Password: ");
            string pass = Console.ReadLine();

            AccountService acService = new AccountService();
            string result = await acService.LoginAsync(userName, pass);

            if (result.ToLower().Contains("user not found") || result.ToLower().Contains("network failed"))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Invalid username and Password.");
                Console.ResetColor();
                return;
            }
            string Fullname = $"{result.Split()[0]} {result.Split()[1]}";
            var acNo = result.Split()[2];
            Console.WriteLine("Loading...");
            string authenticate = await acService.AuthenticationAsync(Fullname);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Account Holder's Name: {Fullname} \nAccount Number: {acNo} \n{authenticate}");
            Console.ResetColor();
        }
    }
}
